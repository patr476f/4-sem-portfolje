﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// A very simplistic car driving on the x-z plane.

public class Drive : MonoBehaviour
{
    public float speed = 10.0f;
    public float rotationSpeed = 100.0f;
    public float accuracyPos = 5.0f;
    public float angSmoothness = 0.02f;

    public GameObject Fuel;

    bool auto = false;

    void Start()
    {

    }

    void LateUpdate()
    {
        // Get the horizontal and vertical axis.
        // By default they are mapped to the arrow keys.
        // The value is in the range -1 to 1
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;

        // Make it move 10 meters per second instead of 10 meters per frame...
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;

        // Move translation along the object's z-axis
        transform.Translate(0, translation, 0);

        // Rotate around our y-axis
        transform.Rotate(0, 0, -rotation);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            CalculateDistance();
            CalculateAngle();
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            auto = !auto;
            Debug.Log("Autopilot: " + auto);
        }
        if (auto)
        {
            AutoPilot();
        }
    }

    void CalculateDistance()
    {
        Vector3 tankpos = this.transform.position;
        Vector3 fuelpos = Fuel.transform.position;
        //will calculate distance between objects, by using Pythagoras Thereom
        float distance = Mathf.Sqrt(
            Mathf.Pow(tankpos.x - fuelpos.x, 2)
            +
            Mathf.Pow(tankpos.y - fuelpos.y, 2)
            +
            Mathf.Pow(tankpos.z - fuelpos.z, 2));

        Debug.Log("Distance: " + distance);

        float unityDistance = Vector3.Distance(tankpos, fuelpos); //Her tages Z med i beregningen
        Debug.Log("UnityDistance: " + unityDistance);
    }

    void CalculateAngle()
    {
        //Method that finds the angle between the facing direction of the tank/object and the vector pointing towards its goal (fuel)
        Vector3 tankFacing = this.transform.up;
        Vector3 fuelDirection = Fuel.transform.position - this.transform.position;

        float dotP = tankFacing.x * fuelDirection.x + tankFacing.y * fuelDirection.y;
        //calculating angle with dot product
        float Angle = Mathf.Acos(
            (dotP) /
            (tankFacing.magnitude * fuelDirection.magnitude)
            ) * Mathf.Rad2Deg; //Acos is Cos^-1 and Since unity is Rad based, Mathf.Rad2Deg is multiplied with the answer to convert to deg

        Debug.Log("Angle: " + Angle);
        Debug.Log("Unity Angle: " + Vector3.Angle(tankFacing, fuelDirection));

        //Good practice to create visual feedback for the vectors, to ensure the correct values are used
        Debug.DrawRay(this.transform.position, tankFacing * 5, Color.green, 2); //tankfacing vector is normalized, so to make it longer it is simply multiplied to help visualization
        Debug.DrawRay(this.transform.position, fuelDirection, Color.red, 2);

        int clockwise = 1;
        if (Cross(tankFacing, fuelDirection).z < 0)
        {
            clockwise = -1;
        }

        float unityAngle = Vector3.SignedAngle(tankFacing, fuelDirection, this.transform.forward);

        //this.transform.Rotate(0, 0, Angle * clockwise);
        this.transform.Rotate(0, 0, unityAngle);
    }

    Vector3 Cross(Vector3 v, Vector3 w)
    {
        float xMult = v.y * w.z - v.z * w.y;
        float yMult = v.z * w.x - v.x * w.z;
        float zMult = v.x * w.y - v.y * w.x;

        Vector3 crossProd = new Vector3(xMult, yMult, zMult);
        return crossProd;
    }

    void AutoPilot()
    {
        //Autopilot method that moves tank towards the goal               
        Vector3 tF = this.transform.up;
        Vector3 fD = Fuel.transform.position - this.transform.position;

        if (fD.magnitude > accuracyPos)
        {
            Debug.DrawRay(this.transform.position, tF * 5, Color.green, 2);
            Debug.DrawRay(this.transform.position, fD, Color.red, 2);

            //first find correct angle for tank
            float angle = Vector3.SignedAngle(tF, fD, this.transform.forward);
            this.transform.Rotate(0, 0, angle * angSmoothness); //angSmoothness helps make the rotation more smooth, by slowly changing the angle instead of rotation the full angle at ones


            //then move object towards the goal
            //transform.Translate(0, speed * Time.deltaTime, 0); //this works
            this.transform.Translate(this.transform.up * speed * Time.deltaTime, Space.World); //solution from guide
        }

    }
}