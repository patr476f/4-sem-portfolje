﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToGoal : MonoBehaviour
{
    public float speed = 2.0f;
    public Transform goal;
    public float accuracy = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //Important that the goal is the first value, as otherwise would move in the opposite direction 
        Vector3 direction = goal.position - this.transform.position;
        this.transform.LookAt(goal.position);

        Debug.DrawRay(this.transform.position, direction, Color.red);

        if (direction.magnitude > accuracy) //Here the magnitude or distance between current position and goal is checked to see if movement is necessary
        {
            //normalized is used to keep the length of the movement at 1, so speed of travel is based on values for speed instead of travel length (with can easily vary)
            this.transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);
        }
    }
}
