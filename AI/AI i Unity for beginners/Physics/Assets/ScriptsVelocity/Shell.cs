﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{
    public GameObject explosion;
    Rigidbody rb;

    // simple acceleration code, not needed with unity features
    //float mass = 10;
    //float force = 300;
    //float acceleration;
    //float speedZ;
    //float gravity = -9.82f;
    //float speedG;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "tank")
        {
            GameObject exp = Instantiate(explosion, this.transform.position, Quaternion.identity);
            Destroy(exp, 0.5f);
            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // simple acc calc, not needed with unity features
        //acceleration = force / mass;
        //speedZ += acceleration * Time.deltaTime;
        //speedG += (gravity / mass) * Time.deltaTime;
        //this.transform.Translate(0, speedG, speedZ);
        //force = 0;

        this.transform.forward = rb.velocity;
    }
}
