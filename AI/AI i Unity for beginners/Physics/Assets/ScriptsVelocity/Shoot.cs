﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject shellPrefab;
    public GameObject shellpawnPos;

    //AI elementer
    public GameObject parent;
    public GameObject target;
    float speed = 15.0f;
    float turnSpeed = 2.0f;
    bool canShoot = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {    
        Vector3 direction = (target.transform.position - parent.transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));

        parent.transform.rotation = Quaternion.Slerp(parent.transform.rotation, lookRotation, Time.deltaTime * turnSpeed);

        float? angle = RotateTurret();

        //checks whether the target can be hit, and is within the view/hitrange of the tank
        if (angle != null && Vector3.Angle(direction, parent.transform.forward) < 10)
        {
            fire();
        }
    }

    void fire()
    {
        if (canShoot)
        {
            GameObject shell = Instantiate(shellPrefab, shellpawnPos.transform.position, shellpawnPos.transform.rotation); //creates a new object with position and rotation of spawn object (cube)
            shell.GetComponent<Rigidbody>().velocity = speed * this.transform.forward;
            canShoot = false;
            Invoke("CanShootAgain", 0.5f); //Invoke calls a method after a set amount of time
        }
        
    }

    void CanShootAgain()
    {
        canShoot = true;
    }

    float? CalculateAngle(bool low)
    {
        //float? allows for returns of either a float or a null
        // the following equation and math is based on finding the roots of a curved trajectory
        //aka don't think too hard about it, it just finds the angle to hit a specific target
        Vector3 targetDir = target.transform.position - this.transform.position;
        float y = targetDir.y;
        targetDir.y = 0f;
        float x = targetDir.magnitude;
        float gravity = 9.82f;
        float sSqr = speed * speed;
        float underTheSqrRoot = (sSqr * sSqr) - gravity * (gravity * x * x + 2 * y * sSqr);

        //if the equation is under 0, then the number is imaginary, which means it can't be hit (and a hassle to calculate with)
        if (underTheSqrRoot >= 0f)
        {
            //since theres two angles, which can hit the same spot, two values are found and choosen based on the method parameter
            float root = Mathf.Sqrt(underTheSqrRoot);
            float highAngle = sSqr + root;
            float lowAngle = sSqr - root;

            if (low)
            {
                return Mathf.Atan2(lowAngle, gravity * x) * Mathf.Rad2Deg;
            }
            else
            {
                return Mathf.Atan2(highAngle, gravity * x) * Mathf.Rad2Deg;
            }
        }
        else
            return null;
    }

    float? RotateTurret()
    {
        float? angle = CalculateAngle(true);

        if (angle!= null)
        {
            this.transform.localEulerAngles = new Vector3(360f - (float)angle, 0f, 0f);
        }

        return angle;
    }
}
