﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathMarker
{
    public MapLocation location;
    public float G;
    public float H;
    public float F;
    public GameObject marker;
    public PathMarker parent;

    public PathMarker(MapLocation l, float g, float h, float f, GameObject marker, PathMarker p)
    {
        location = l;
        G = g;
        H = h;
        F = f;
        this.marker = marker;
        parent = p;
    }

    public override bool Equals(object obj)
    {
        //checks whether the object is equal to another object
        if (obj == null || !this.GetType().Equals(this.GetType())) //standard check making sure it same type or actual object
        {
            return false;
        }
        else
            return location.Equals(((PathMarker)obj).location); //if location isn't the same, the object isn't true
    }

    public override int GetHashCode()
    {
        return 0;
    }
}

public class FindPathAStar : MonoBehaviour
{
    public Maze maze;
    public Material closedMaterial;
    public Material openMaterial;

    List<PathMarker> open = new List<PathMarker>();
    List<PathMarker> closed = new List<PathMarker>();

    public GameObject start;
    public GameObject end;
    public GameObject path;

    PathMarker goalNode;
    PathMarker startNode;

    PathMarker lastPos;
    bool done = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            BeginSearch();
        }
        if (Input.GetKeyDown(KeyCode.W) && !done)
        {
            Search(lastPos);
        }
        if (Input.GetKeyDown(KeyCode.E) && done)
        {
            GetPath();
        }
    }

    void RemoveAllMarkers()
    {
        GameObject[] markers = GameObject.FindGameObjectsWithTag("marker");
        foreach (GameObject m in markers)
        {
            Destroy(m);
        }
    }

    void BeginSearch()
    {
        //resets maze for markers and path bool
        done = false;
        RemoveAllMarkers();

        //all locations that aren't walls are added to a list, walls are set to 1 so any value other than 1 isn't walls
        List<MapLocation> locations = new List<MapLocation>();
        for (int z = 1; z < maze.depth; z++)
        {
            for (int x = 1; x < maze.width; x++)
            {
                if (maze.map[x, z] != 1)
                {
                    locations.Add(new MapLocation(x, z));
                }
            }
        }

        //list is shuffled to allow random pick of locations for start and end
        locations.Shuffle();

        //start and goal is picked by taking 1st and 2nd values of the list (randomized list)
        Vector3 startLocation = new Vector3(locations[0].x * maze.scale, 0, locations[0].z * maze.scale);
        startNode = new PathMarker(new MapLocation(locations[0].x, locations[0].z), 0, 0, 0,
            Instantiate(start, startLocation, Quaternion.identity), null);

        Vector3 goalLocation = new Vector3(locations[1].x * maze.scale, 0, locations[1].z * maze.scale);
        goalNode = new PathMarker(new MapLocation(locations[1].x, locations[1].z), 0, 0, 0,
            Instantiate(end, goalLocation, Quaternion.identity), null);

        //open and closed list are reset and start location is set
        open.Clear();
        closed.Clear();
        open.Add(startNode);
        lastPos = startNode;
    }

    void Search(PathMarker thisNode)
    {
        if (thisNode.Equals(goalNode))
        {
            Debug.Log("testDone");
            done = true;
            return; //goal reached/found
        }

        foreach (MapLocation dir in maze.directions)
        {
            MapLocation neighbor = dir + thisNode.location;

            //checks for fail conditions
            if (maze.map[neighbor.x, neighbor.z] == 1)
            {
                Debug.Log("testWall");
                continue;
            }
            if (neighbor.x >= maze.width || neighbor.x < 1 || neighbor.z >= maze.depth || neighbor.z < 1)
            {
                Debug.Log("testOut");
                continue;
            }
            if (IsClosed(neighbor))
            {
                Debug.Log("testClosed");
                continue;
            }

            //finds G, H & F for neighbor
            float G = Vector2.Distance(thisNode.location.ToVector(), neighbor.ToVector()) + thisNode.G;
            float H = Vector2.Distance(neighbor.ToVector(), goalNode.location.ToVector());
            float F = G + H;

            //creates new gameobject
            GameObject pathBlock = Instantiate(path, new Vector3(neighbor.x * maze.scale, 0, neighbor.z * maze.scale), Quaternion.identity);

            TextMesh[] values = pathBlock.GetComponentsInChildren<TextMesh>();
            values[0].text = "G: " + G.ToString("0.00");
            values[1].text = "H: " + H.ToString("0.00");
            values[2].text = "F: " + F.ToString("0.00");

            if (!UpdateMarker(neighbor, G, H, F, thisNode))
            {
                open.Add(new PathMarker(neighbor, G, H, F, pathBlock, thisNode));
            }
        }

        //find next processing marker
        open = open.OrderBy(path => path.F).ToList<PathMarker>();
        PathMarker pm = (PathMarker)open.ElementAt(0);
        closed.Add(pm);

        open.RemoveAt(0);
        pm.marker.GetComponent<Renderer>().material = closedMaterial;

        lastPos = pm;
    }

    bool IsClosed(MapLocation marker)
    {
        foreach (PathMarker p in closed)
        {
            if (p.location.Equals(marker))
            {
                return true;
            }
        }
        return false;
    }

    bool UpdateMarker(MapLocation pos, float g, float h, float f, PathMarker prt)
    {
        foreach (PathMarker p in open)
        {
            if (p.location.Equals(pos))
            {
                p.G = g;
                p.H = h;
                p.F = f;
                p.parent = prt;
                return true;
            }
        }
        return false;
    }

    void GetPath()
    {
        RemoveAllMarkers();
        PathMarker begin = lastPos;

        while (!begin.Equals(startNode) && begin != null)
        {
            Instantiate(path, new Vector3(begin.parent.location.x * maze.scale, 0, begin.parent.location.z * maze.scale), Quaternion.identity);

            begin = begin.parent;
        }

        Instantiate(start, new Vector3(startNode.location.x * maze.scale, 0, startNode.location.z * maze.scale), Quaternion.identity);
        Instantiate(end, new Vector3(goalNode.location.x * maze.scale, 0, goalNode.location.z * maze.scale), Quaternion.identity);
    }
}
