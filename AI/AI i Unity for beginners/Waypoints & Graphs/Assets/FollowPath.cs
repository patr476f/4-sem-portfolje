﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    Transform goal;
    float speed = 5.0f;
    float accuracy = 1.0f;
    float rotSpeed = 2.0f;
    public GameObject wpManager;
    GameObject[] wps;
    GameObject currentNode; //the current node which AI is trying to get to
    int currentWP = 0; //Node index for the path calculated by A*
    Graph g;

    // Start is called before the first frame update
    void Start()
    {
        wps = wpManager.GetComponent<WPManager>().waypoints;
        g = wpManager.GetComponent<WPManager>().graph;
        currentNode = wps[1]; //the index should correspond with the waypoint the tank is currently located at when starting the game
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void LateUpdate()
    {
        //checks wether the AI is at the goal or if any path is set (path = 0)
        if (g.getPathLength() == 0 || currentWP == g.getPathLength())
        {
            return;
        }

        currentNode = g.getPathPoint(currentWP); //currentNode is set to match the currentWP node for the path

        //if the AI is near enough the next WP, the next WP is set by increasing the index (currentWP)
        if (Vector3.Distance(g.getPathPoint(currentWP).transform.position, transform.position) < accuracy)
        {
            currentWP++;
        }

        if (currentWP < g.getPathLength())
        {
            goal = g.getPathPoint(currentWP).transform;

            Vector3 lookAtGoal = new Vector3(goal.position.x, this.transform.position.y, goal.position.z);
            Vector3 direction = lookAtGoal - this.transform.position;

            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);
            this.transform.Translate(0, 0, speed * Time.deltaTime);
        }
    }

    public void GoToHeli()
    {
        g.AStar(currentNode, wps[2]);
        currentWP = 0;
    }

    public void GoToRuin()
    {
        g.AStar(currentNode, wps[11]);
        currentWP = 0;
    }

    public void GoToTanks()
    {
        g.AStar(currentNode, wps[7]);
        currentWP = 0;
    }

    public void GoToBarrack()
    {
        g.AStar(currentNode, wps[0]);
        currentWP = 0;
    }
}
