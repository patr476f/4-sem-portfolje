﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowWP : MonoBehaviour
{

    public GameObject[] waypoints;
    int currentWP = 0;

    public float speed = 10.0f;
    public float rotSpeed = 10.0f;
    public float lookAhead = 10.0f; //tells how far ahead the tracker will move based on the tank

    GameObject tracker; //this tracker will be the guide for the tank/AI, helping navigate the track without getting stuck
    [SerializeField]
    bool trackerRender = false;

    // Start is called before the first frame update
    void Start()
    {
        //the tracker is created with same properties as the tank, collider is removed to ensure its only a guideline for the tank
        tracker = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        DestroyImmediate(tracker.GetComponent<Collider>());
        tracker.transform.position = this.transform.position;
        tracker.transform.rotation = this.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        tracker.GetComponent<MeshRenderer>().enabled = trackerRender;
        ProgressTracker();

        //this.transform.LookAt(waypoints[currentWP].transform);

        //method to smooth rotation between waypoints instead of snapping of LookAt, in order to create more realistic movement
        Quaternion lookAtWP = Quaternion.LookRotation(tracker.transform.position - this.transform.position);
        this.transform.rotation = Quaternion.Slerp(transform.rotation, lookAtWP, rotSpeed * Time.deltaTime);

        this.transform.Translate(0, 0, speed * Time.deltaTime);
    }

    void ProgressTracker()
    {
        if (Vector3.Distance(tracker.transform.position, this.transform.position) > lookAhead)
        {
            return; //stops the method if the tracker is too far ahead
        }

        //this method helps the tracker navigate the track, following the same logic as the tank
        if (Vector3.Distance(tracker.transform.position, waypoints[currentWP].transform.position) < 3)
        {
            currentWP++;
        }

        if (currentWP >= waypoints.Length)
        {
            currentWP = 0;
        }

        tracker.transform.LookAt(waypoints[currentWP].transform.position); //instead of the smooth rotation of the tank, the tracker only needs to move between waypoints

        tracker.transform.Translate(0, 0, (speed + 20) * Time.deltaTime); //the speed of the tracker is set bearing in mind that the tracker and tank can't be too far away from eachother
        //if the tank moves too far away, it will start taking 'shortcuts' and skip part of the track in order to keep up with the tracker
        // this can cause problems if you want the tank to stay on a specific track, or if there's terrain between waypoints
    }
}
