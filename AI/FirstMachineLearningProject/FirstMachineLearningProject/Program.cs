﻿using Microsoft.ML;
using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FirstMachineLearningProject
{
    class Program
    {
        static void Main(string[] args)
        {
            #region oldversion
            //Commented out to allow new version to be made within same project
            ////needed for ML functionality (its the context for all the functions)
            //var context = new MLContext();

            //var textLoader = context.Data.CreateTextLoader(
            //new TextLoader.Options()
            //{
            //    Separators = new[] { ',' },
            //    HasHeader = true,
            //    AllowQuoting = true,
            //    Columns = new[]
            //    {
            //        new TextLoader.Column("Label", DataKind.Boolean, 1),
            //        new TextLoader.Column("Pclass", DataKind.Single, 2),
            //        new TextLoader.Column("Name", DataKind.String, 3),
            //        new TextLoader.Column("Sex", DataKind.String, 4),
            //        new TextLoader.Column("Age", DataKind.Single, 5),
            //        new TextLoader.Column("SibSp", DataKind.Single, 6),
            //        new TextLoader.Column("Parch", DataKind.Single, 7),
            //        new TextLoader.Column("Ticket", DataKind.String, 8),
            //        new TextLoader.Column("Fare", DataKind.Single, 9),
            //        new TextLoader.Column("Cabin", DataKind.String, 10),
            //        new TextLoader.Column("Embarked", DataKind.String, 11)
            //    }
            //});

            //var trainingData = textLoader.Load("./data/train.csv");
            //var split = context.Data.TrainTestSplit(trainingData, testFraction: 0.2);

            //var testData = textLoader.Load("./data/test.csv");

            //var pipeline = context.Transforms.DropColumns("Name", "Cabin", "Ticket")
            //    .Append(context.Transforms.Categorical.OneHotEncoding("Sex"))
            //    .Append(context.Transforms.Categorical.OneHotEncoding("Embarked"))
            //    .Append(context.Transforms.Concatenate(
            //        "Features",
            //        "Age",
            //        "Pclass",
            //        "SibSp",
            //        "Parch",
            //        "Sex",
            //        "Embarked"))
            //    .Append(context.BinaryClassification.Trainers.FastTree());

            //var model = pipeline.Fit(split.TrainSet);
            //var predictions = model.Transform(split.TestSet);

            //var metrics = context.BinaryClassification.Evaluate(predictions);

            //Console.WriteLine($"R^2 = {metrics.Accuracy}");
            #endregion oldversion

            var context = new MLContext();

            var trainData = context.Data.LoadFromTextFile<Input>("./data/train.csv", hasHeader: true, separatorChar: ',', allowQuoting: true);
            var testData = context.Data.LoadFromTextFile<Input2>("./data/test.csv", hasHeader: true, separatorChar: ',', allowQuoting: true); //May be redundant

            ////debugging field, can be removed when project is fully completed/finalized
            //var debug1 = trainData.Preview();
            //var debug2 = testData.Preview(); //Test-data can't currently be loaded, as errors occurs and finding the actual cause is near impossible
            //var rowTest = testData.GetRowCount(); //field to find method to get row-count for future loop setup (may be removed/unneeded if another method is found)

            //splitting trainning data, to help give evaluation (can be removed to train more effeciently)
            var split = context.Data.TrainTestSplit(trainData, testFraction: 0.1);

            //Fast tree
            var pipelineFastTree = context.Transforms.Categorical.OneHotEncoding("Sex")
                .Append(context.Transforms.Categorical.OneHotEncoding("Embarked"))
                .Append(context.Transforms.Concatenate("Features", new[] { "Sex", "Pclass", "Age", "SibSp", "Parch", "Fare", "Embarked" }))
                .Append(context.Transforms.ReplaceMissingValues("NewValues", "Features",
                Microsoft.ML.Transforms.MissingValueReplacingEstimator.ReplacementMode.Mode))
                .Append(context.BinaryClassification.Trainers.FastTree());

            var pipelineLightGbm = context.Transforms.Categorical.OneHotEncoding("Sex")
                .Append(context.Transforms.Categorical.OneHotEncoding("Embarked"))
                .Append(context.Transforms.Concatenate("Features", new[] { "Sex", "Pclass", "Age", "SibSp", "Parch", "Fare", "Embarked" }))
                .Append(context.Transforms.ReplaceMissingValues("NewValues", "Features",
                Microsoft.ML.Transforms.MissingValueReplacingEstimator.ReplacementMode.Mode))
                .Append(context.BinaryClassification.Trainers.LightGbm());

            //var model = pipeline.Fit(trainData);
            var modelFastTree = pipelineFastTree.Fit(split.TrainSet);
            var modelLightGbm = pipelineLightGbm.Fit(split.TrainSet);

            #region evalutation
            //predictions
            var predictions = modelFastTree.Transform(split.TestSet);
            //var predictions = model.Transform(testData); //Doesn't work as test.csv doesn't have the actual survived value and is only for submitting the predicted scores/values on Kaggle
            var metrics = context.BinaryClassification.Evaluate(predictions);

            Console.WriteLine("Fast Tree:");
            Console.WriteLine($"Acc: {metrics.Accuracy}");
            Console.WriteLine($"F1: {metrics.F1Score}");

            Console.WriteLine("-------------------------");

            predictions = modelLightGbm.Transform(split.TestSet);
            metrics = context.BinaryClassification.Evaluate(predictions);

            Console.WriteLine("Light Gbm:");
            Console.WriteLine($"Acc: {metrics.Accuracy}");
            Console.WriteLine($"F1: {metrics.F1Score}");

            Console.WriteLine("-------------------------");
            #endregion evalutation

            //Creation of predictor, to see prediction based outcomes
            var predictorFastTree = context.Model.CreatePredictionEngine<Input2, Output>(modelFastTree); //Input2 is used to enable future prediction of test.csv data
            var predictorLightGbm = context.Model.CreatePredictionEngine<Input2, Output>(modelLightGbm);

            #region hardcode prediction
            var newPas = new Input2
            {
                PassengerId = 232,
                Pclass = 1,
                Sex = "male",
                Age = 53,
                SibSp = 3,
                Parch = 0,
                Fare = 8.26f,
                Embarked = "S"
            };

            var prediction = predictorFastTree.Predict(newPas);

            Console.WriteLine(prediction.PassengerId);
            Console.WriteLine("Survived: " + Convert.ToInt32(prediction.Survived));
            Console.WriteLine("Probability: " + prediction.Probability);
            Console.WriteLine("Score: " + prediction.Score);

            Console.WriteLine("-----------------------");
            prediction = predictorLightGbm.Predict(newPas);

            Console.WriteLine(prediction.PassengerId);
            Console.WriteLine("Survived: " + Convert.ToInt32(prediction.Survived));
            Console.WriteLine("Probability: " + prediction.Probability);
            Console.WriteLine("Score: " + prediction.Score);
            #endregion hardcode prediction

            /*
             * Comments on the current projekt 01-03-2021:
             * Training and prediction work, however problems arise when trying to load test-data.
             * This problem can be caused by the lack of survived column, or simply bad/misunderstood use of ML.NET.
             * The problem will be brought up next ERFA meeting, where solutions may be found by other members who use ML.NET.
             * 
             * !*!
             * Somewhat solution found, by having seperat class to load data, no errors occurs.
             * This currently indicate that ML.NET can't load different .csv files if the columns doesn't match.
             */

            /*
             * Notes for submission:
             * For the Kaggle submission items, you need to style is like gender_submission.csv.
             * Where PassengerId and survived is kept only.
             */

            #region streamReader Data
            /*
             * This code may work, but https://www.youtube.com/watch?v=w67dNSulQTE may be a better solution
             * as it uses ML.NET functions to replace the values. This can also be done on the actual training data for better results.
             * But currently this solution is fine for first iteration of Titanic.
             */
            List<Input2> testArray = new List<Input2>();
            bool first = true;
            using (var reader = new StreamReader("./data/test.csv"))
            {
                while (!reader.EndOfStream)
                {


                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    if (first)
                    {
                        first = false;
                        continue;
                    }

                    float n;
                    //values position of sex (and all after) have added 1, as name adds 1 more column
                    var testInput = new Input2
                    {
                        PassengerId = float.Parse(values[0]),
                        Pclass = float.Parse(values[1]),
                        Sex = values[4],
                        Age = float.TryParse(values[5], out n) ? n : 0,
                        SibSp = float.Parse(values[6]),
                        Parch = float.Parse(values[7]),
                        Fare = float.TryParse(values[9], out n) ? n : 0
                    };

                    testArray.Add(testInput);
                }
            }
            Console.WriteLine(testArray.Count);
            #endregion StreamReader Data

            #region prediction of test.csv
            string pathFastTree = "./data/predictionFastTree.csv";
            if (File.Exists(pathFastTree))
            {
                File.Delete(pathFastTree);
            }

            first = true; //for first cycle in loop
            foreach (Input2 passenger in testArray)
            {
                var testPredict = predictorFastTree.Predict(passenger);

                try
                {
                    using (StreamWriter file = new StreamWriter(pathFastTree, true))
                    {
                        if (first)
                        {
                            first = false;
                            file.WriteLine("PassengerId,Survived");
                        }
                        //survived needs to be set as 0 or 1, for Kaggle submissions
                        file.WriteLine(testPredict.PassengerId + "," + Convert.ToInt32(testPredict.Survived));
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("This program failed:" + ex);
                }
            }
            #endregion prediction of test.csv

            #region prediction light gbm
            string pathLightGbm = "./data/predictionLightGbm.csv";
            if (File.Exists(pathLightGbm))
            {
                File.Delete(pathLightGbm);
            }

            first = true; //for first cycle in loop
            foreach (Input2 passenger in testArray)
            {
                var testPredict = predictorLightGbm.Predict(passenger);

                try
                {
                    using (StreamWriter file = new StreamWriter(pathLightGbm, true))
                    {
                        if (first)
                        {
                            first = false;
                            file.WriteLine("PassengerId,Survived");
                        }
                        //survived needs to be set as 0 or 1, for Kaggle submissions
                        file.WriteLine(testPredict.PassengerId + "," + Convert.ToInt32(testPredict.Survived));
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("This program failed:" + ex);
                }
            }
            #endregion prediction light gbm
        }

    }

    public class Input
    {
        [LoadColumn(1), ColumnName("Label")]
        public bool Survived { get; set; }

        [LoadColumn(2)]
        public float Pclass { get; set; }

        [LoadColumn(4)]
        public string Sex { get; set; }

        [LoadColumn(5)]
        public float Age { get; set; }

        [LoadColumn(6)]
        public float SibSp { get; set; }

        [LoadColumn(7)]
        public float Parch { get; set; }

        [LoadColumn(9)]
        public float Fare { get; set; }

        [LoadColumn(11)]
        public string Embarked { get; set; }
    }

    public class Input2
    {
        [LoadColumn(0), ColumnName("PassengerId")]
        public float PassengerId { get; set; }

        [LoadColumn(1)]
        public float Pclass { get; set; }

        [LoadColumn(3)]
        public string Sex { get; set; }

        [LoadColumn(4)]
        public float Age { get; set; }

        [LoadColumn(5)]
        public float SibSp { get; set; }

        [LoadColumn(6)]
        public float Parch { get; set; }

        [LoadColumn(8)]
        public float Fare { get; set; }

        [LoadColumn(10)]
        public string Embarked { get; set; }
    }

    public class Output
    {
        [ColumnName("PassengerId")]
        public float PassengerId { get; set; }

        [ColumnName("PredictedLabel")]
        public bool Survived { get; set; }

        public float Probability { get; set; }
        public float Score { get; set; }
    }

}
