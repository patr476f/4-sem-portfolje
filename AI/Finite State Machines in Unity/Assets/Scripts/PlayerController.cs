﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //This script is based on a non-FSM method of creating player controls/states in a game.
    //This method of managing states is also called Naive Approach, or atleast that term is used in the course
    #region Player Variables

    public float jumpForce;
    public Transform head;
    public Transform weapon01;
    public Transform weapon02;

    public Sprite idleSprite;
    public Sprite duckingSprite;
    public Sprite jumpingSprite;
    public Sprite spinningSprite;

    private SpriteRenderer face;
    private Rigidbody rbody;

    private bool isJumping;
    private bool isDucking;
    private bool isSpinning;

    //variable to unsure rotation is saved between frames
    private float rotation;

    #endregion

    private void Awake()
    {
        face = GetComponentInChildren<SpriteRenderer>();
        rbody = GetComponent<Rigidbody>();
        SetExpression(idleSprite);
    }

    // Update is called once per frame
    void Update()
    {
        //Checks for the Jump button, unsure if this is predefined as space or set previously in the demo
        //inputs can be found in Edit > Project Manager > Input, here are all the inputs for the project and the names given
        // to the specific inputs.
        if (Input.GetButtonDown("Jump"))
        {
            //used to ensure no double jumping/infinite jumping happens
            if (isJumping == false && isDucking == false)
            {
                isJumping = true;
                SetExpression(jumpingSprite);
                rbody.AddForce(Vector3.up * jumpForce);
            }            
        }
        else if (Input.GetButtonDown("Duck"))
        {
            if (isJumping == false)
            {
                isDucking = true;
                head.localPosition = new Vector3(head.localPosition.x, .5f, head.localPosition.z);
                SetExpression(duckingSprite);
            }
            else
            {
                //isSpinning is called here, since Spin() needs to be called over multiple frames (which isn't possible within the if statement of Duck input)
                isSpinning = true;
                SetExpression(spinningSprite);
            }
        }
        else if (Input.GetButtonUp("Duck"))
        {
            if (isJumping == false)
            {
                isDucking = false;
                head.localPosition = new Vector3(head.localPosition.x, .8f, head.localPosition.z);
                SetExpression(idleSprite);
            }
        }
        else if (Input.GetButtonDown("SwapWeapon"))
        {
            if (!isJumping && !isDucking && !isSpinning)
            {
                bool usingWeapon01 = weapon01.gameObject.activeInHierarchy;

                weapon01.gameObject.SetActive(usingWeapon01 == false);
                weapon02.gameObject.SetActive(usingWeapon01);
            }
        }

        if (isSpinning == true)
        {
            Spin();
        }
    }

    private void Spin()
    {
        //rotation method
        //amount of rotation done
        float amountToRotate = 900 * Time.deltaTime;
        rotation += amountToRotate;

        //specifics for what happens here is unimportant, it just makes sure the spin is done correctly
        //for future reference learn more about models and their transformation/movement in Unity.
        if (rotation < 360)
        {
            transform.Rotate(Vector3.up, amountToRotate);
        }
        else
        {
            transform.rotation = Quaternion.identity;
            isSpinning = false;
            rotation = 0;
            SetExpression(jumpingSprite);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //OnCollisionEnter runs when player hits ground
        //It's a Unity event function and detects collision with the ground
        isJumping = false;
        SetExpression(idleSprite);
    }

    public void SetExpression(Sprite newExpression)
    {
        face.sprite = newExpression;
    }
}
