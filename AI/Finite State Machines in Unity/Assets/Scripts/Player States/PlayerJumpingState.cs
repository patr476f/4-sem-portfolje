﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJumpingState : PlayerBaseState
{
    public override void EnterState(PlayerController_FSM player)
    {
        player.SetExpression(player.jumpingSprite);
    }

    public override void OnCollisionEnter(PlayerController_FSM player)
    {
        player.TransitionToState(player.IdleState);
    }

    public override void Update(PlayerController_FSM player)
    {
        if (Input.GetButtonDown("Duck"))
        {
            //a new instance is created here instead of one in the main class (PlayerController_FSM)
            //This is done since the spin needs to be reset each call of its state.
            //The state needs a rotation variable, which increase with its use, so to ensure its always reset new instances are created instead of reusing one.
            player.TransitionToState(new PlayerSpinningState());
        }
    }
}
