﻿using UnityEngine;

public abstract class PlayerBaseState
{
    //This is the abstract class that holds the common behaviors for the system/states
    //Remember that the parameter isn't necessarily needed in FSM pattern, but only for methods that require them for uses.
    //Which is all of them in this example.
    //The Parameters can therefore be of any types or have no parameters.
    public abstract void EnterState(PlayerController_FSM player);

    public abstract void Update(PlayerController_FSM player);

    public abstract void OnCollisionEnter(PlayerController_FSM player);
}
