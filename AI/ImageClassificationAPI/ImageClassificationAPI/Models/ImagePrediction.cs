﻿namespace ImageClassificationAPI.Models
{
    public class ImagePrediction : ImageData
    {
        //this class is for the prediction of images, for holding the scores and predicted label
        public float[] Score { get; set; } //scores are defined by the different labels given during training
        //the scores and associated labels correspond to the appearence in the training tags file

        public string PredictedLabelValue { get; set; }
    }
}
