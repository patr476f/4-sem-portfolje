﻿namespace ImageClassificationAPI.Models
{
    public class ImageRespons
    {
        public float Accuracy { get; set; }
        public string PredictedLabel { get; set; }
    }
}
