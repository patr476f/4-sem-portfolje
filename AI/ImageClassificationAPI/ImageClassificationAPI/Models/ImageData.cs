﻿using Microsoft.ML.Data;

namespace ImageClassificationAPI.Models
{
    public class ImageData
    {
        [LoadColumn(0)]
        public string ImagePath { get; set; } //the path for the image (used for training)

        [LoadColumn(1)]
        public string Label { get; set; } //the classification for the image
    }
}
