﻿using ImageClassificationAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.ML;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace ImageClassificationAPI.Controllers
{
    [ApiController]
    [Route("image")]
    public class ImageClassController : Controller
    {
        //paths for training
        static readonly string _assetsPath = Path.Combine(Environment.CurrentDirectory, "Assets");
        static readonly string _trainingFolder = Path.Combine(_assetsPath, "Training");
        static readonly string _imagesFolder = Path.Combine(_trainingFolder, "Images");

        //paths for request handling
        static readonly string _requestFolder = Path.Combine(Environment.CurrentDirectory, "RequestImage");
        static readonly string _requestImage = Path.Combine(_requestFolder, "requestImage.jpg");

        //this tags contains the training labels and images
        //if more images is needed for training remember to add them in this .tsv file
        static readonly string _trainTagsTsv = Path.Combine(_trainingFolder, "train-tags.tsv");
        static readonly string _testTagsTsv = Path.Combine(_trainingFolder, "test-tags.tsv");

        //saved model path
        static readonly string _savedModel = Path.Combine(_assetsPath, "SavedModel.zip");

        //inception model path
        static readonly string _inceptionTensorFlowModel = Path.Combine(_assetsPath, "inception", "tensorflow_inception_graph.pb");

        //settings for the inception model
        private struct InceptionSettings
        {
            public const int ImageHeight = 224;
            public const int ImageWidth = 224;
            public const float Mean = 117;
            public const float Scale = 1;
            public const bool ChannelsLast = true;
        }

        MLContext mlContext = new MLContext();
        ITransformer currentModel;

        public ImageClassController()
        {
            if (System.IO.File.Exists(_savedModel))
            {
                LoadModel();
            }
                    
        }

        [HttpPost]
        public ImageRespons ImageClassification(IFormFile image)
        {
            //This method takes a image input and returns a prediction of the image
            ImageRespons respons = new ImageRespons();

            try
            {
                //saves image to the local path, in order to get file path
                using (var fileStream = new FileStream(_requestImage, FileMode.Create))
                {
                    image.CopyTo(fileStream);
                }

                var imageData = new ImageData
                {
                    ImagePath = _requestImage
                };

                var predictor = mlContext.Model.CreatePredictionEngine<ImageData, ImagePrediction>(currentModel);
                ImagePrediction prediction = predictor.Predict(imageData);

                respons.Accuracy = prediction.Score.Max();
                respons.PredictedLabel = prediction.PredictedLabelValue;
            }
            catch (Exception)
            {
                return null;
            }

            return respons;
        }

        [HttpGet("generate")]
        public IEnumerable<ImagePrediction> GenerateModel()
        {
            //method generates the model
            IEstimator<ITransformer> pipeline = mlContext.Transforms.LoadImages(outputColumnName: "input", imageFolder: _imagesFolder, inputColumnName: nameof(ImageData.ImagePath))
                // The image transforms transform the images into the model's expected format.
                .Append(mlContext.Transforms.ResizeImages(outputColumnName: "input", imageWidth: InceptionSettings.ImageWidth, imageHeight: InceptionSettings.ImageHeight, inputColumnName: "input"))
                .Append(mlContext.Transforms.ExtractPixels(outputColumnName: "input", interleavePixelColors: InceptionSettings.ChannelsLast, offsetImage: InceptionSettings.Mean))
                .Append(mlContext.Model.LoadTensorFlowModel(_inceptionTensorFlowModel).
                    ScoreTensorFlowModel(outputColumnNames: new[] { "softmax2_pre_activation" }, inputColumnNames: new[] { "input" }, addBatchDimensionInput: true))
                .Append(mlContext.Transforms.Conversion.MapValueToKey(outputColumnName: "LabelKey", inputColumnName: "Label"))
                .Append(mlContext.MulticlassClassification.Trainers.LbfgsMaximumEntropy(labelColumnName: "LabelKey", featureColumnName: "softmax2_pre_activation"))
                .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabelValue", "PredictedLabel"))
                .AppendCacheCheckpoint(mlContext);

            IDataView trainingData = mlContext.Data.LoadFromTextFile<ImageData>(path: _trainTagsTsv, hasHeader: false);
            currentModel = pipeline.Fit(trainingData);

            //Testing of the model, may not be possible/necessary as it's on an API
            IDataView testData = mlContext.Data.LoadFromTextFile<ImageData>(path: _testTagsTsv, hasHeader: false);
            IDataView predictions = currentModel.Transform(testData);
            IEnumerable<ImagePrediction> imagePredictionData = mlContext.Data.CreateEnumerable<ImagePrediction>(predictions, true);

            SaveModel(currentModel, trainingData);

            //debug loop to check items in list
            foreach (ImagePrediction item in imagePredictionData)
            {
                var path = item.ImagePath;
            }

            return imagePredictionData;
        }

        private void LoadModel()
        {
            //loads the saved model from files
            DataViewSchema modelSchema;

            currentModel = mlContext.Model.Load(_savedModel, out modelSchema);
        }

        private void SaveModel(ITransformer model, IDataView data)
        {
            //saves the current model to files
            mlContext.Model.Save(model, data.Schema, _savedModel);
        }
    }
}
