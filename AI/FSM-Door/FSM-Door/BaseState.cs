﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FSM_Door
{
    public abstract class BaseState
    {
        public abstract void EnterState();
        public abstract void Update(Door door);
    }
}
