﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FSM_Door
{
    public class LockedState : BaseState
    {
        public override void EnterState()
        {
            Console.WriteLine("The Door is Locked...");
        }

        public override void Update(Door door)
        {
            if (door.action.Key == ConsoleKey.D2)
            {
                Console.WriteLine("Unlocking the Door...");
                door.TransitionState(door.closedState);
            }
            else
            {
                Console.WriteLine("Nothing happens...");
            }
        }
    }
}
