﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FSM_Door
{
    public class Door
    {
        public BaseState ActiveState;

        public readonly ClosedState closedState = new ClosedState();
        public readonly OpenState openState = new OpenState();
        public readonly LockedState lockedState = new LockedState();

        public bool Loop = true;
        public ConsoleKeyInfo action;

        public Door()
        {
            Start();
        }

        private void Start()
        {
            TransitionState(closedState);

            Update();
        }

        public void TransitionState(BaseState state)
        {
            ActiveState = state;

            ActiveState.EnterState();
        }

        public void Update()
        {
            while (Loop)
            {
                Console.WriteLine("----------------------------");
                Console.WriteLine("Select Action:" +
                    "\n" +
                    "1. Open/Close \n" +
                    "2. Lock/Unlock \n" +
                    "Esc to close application");
                action = Console.ReadKey();
                
                Console.WriteLine();

                if (action.Key == ConsoleKey.Escape)
                {
                    Loop = false;
                    break;
                }

                ActiveState.Update(this);
            }
        }

    }
}
