﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FSM_Door
{
    public class ClosedState : BaseState
    {
        public override void EnterState()
        {
            Console.WriteLine("The Door is Closed...");
        }

        public override void Update(Door door)
        {
            if (door.action.Key == ConsoleKey.D1)
            {
                Console.WriteLine("Opening the door...");
                door.TransitionState(door.openState);
            }
            else if (door.action.Key == ConsoleKey.D2)
            {
                Console.WriteLine("Locking the door...");
                door.TransitionState(door.lockedState);
            }
            else
            {
                Console.WriteLine("Nothing happens...");
            }

        }
    }
}
