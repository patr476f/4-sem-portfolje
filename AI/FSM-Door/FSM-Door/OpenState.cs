﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FSM_Door
{
    public class OpenState : BaseState
    {
        public override void EnterState()
        {
            Console.WriteLine("The Door is Open...");
        }

        public override void Update(Door door)
        {
            if (door.action.Key == ConsoleKey.D1)
            {
                Console.WriteLine("Closing the Door...");
                door.TransitionState(door.closedState);
            }
            else
            {
                Console.WriteLine("Nothing happens...");
            }
        }
    }
}
