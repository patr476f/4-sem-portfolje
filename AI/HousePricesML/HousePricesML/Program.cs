﻿using System;
using System.Collections.Generic;
using System.IO;
using HousePricesML.DataClasses;
using Microsoft.ML;

namespace HousePricesML
{
    class Program
    {
        static void Main(string[] args)
        {
            //context for ML.NET
            var context = new MLContext();

            //properties
            string trainingFilePath = "./Data/train.csv";
            var features = new[] {
                "MSSubClass",
                "MSZoning",
                "LotArea",
                "Street",
                "Alley",
                "Utilities",
                "Neighborhood",
                "Condition1",
                "BldgType",
                "HouseStyle",
                "OverallQual",
                "OverallCond",
                "YearBuilt",
                "YearRemodAdd",
                "ExterQual",
                "ExterCond",
                "BsmtQual",
                "BsmtCond",
                "Heating",
                "Electrical",
                "KitchenQual",
                "MoSold",
                "YrSold",
                "SaleType",
                "SaleCondition"
            }; //array with all the feature columns
            var stringColumns = new[] {
                new InputOutputColumnPair("MSZoning"),
                new InputOutputColumnPair("Street"),
                new InputOutputColumnPair("Alley"),
                new InputOutputColumnPair("Utilities"),
                new InputOutputColumnPair("Neighborhood"),
                new InputOutputColumnPair("Condition1"),
                new InputOutputColumnPair("BldgType"),
                new InputOutputColumnPair("HouseStyle"),
                new InputOutputColumnPair("ExterQual"),
                new InputOutputColumnPair("ExterCond"),
                new InputOutputColumnPair("BsmtQual"),
                new InputOutputColumnPair("BsmtCond"),
                new InputOutputColumnPair("Heating"),
                new InputOutputColumnPair("HeatingQC"),
                new InputOutputColumnPair("Electrical"),
                new InputOutputColumnPair("KitchenQual"),
                new InputOutputColumnPair("SaleType"),
                new InputOutputColumnPair("SaleCondition")
            }; //array with all the string columns, needed for OneHotEncoding

            //Loading the training data from .csv file
            var trainData = context.Data.LoadFromTextFile<InputTrain>(trainingFilePath, hasHeader: true, separatorChar: ',');

            //splitting data for training purposes
            var split = context.Data.TrainTestSplit(trainData, testFraction: 0.1);

            //pipeline created
            var pipeLine = context.Transforms.Categorical.OneHotEncoding(stringColumns)
                .Append(context.Transforms.Concatenate("Features", features))
                .Append(context.Transforms.ReplaceMissingValues("NewValues", "Features",
                Microsoft.ML.Transforms.MissingValueReplacingEstimator.ReplacementMode.Mode));

            //trainer added to pipeline
            var trainerFastTree = pipeLine.Append(context.Regression.Trainers.FastTree());

            //Model
            var modelFastTree = trainerFastTree.Fit(split.TrainSet);

            #region Evaluation
            //prediction
            var fastTreePredictions = modelFastTree.Transform(split.TestSet);
            var metrics = context.Regression.Evaluate(fastTreePredictions);

            Console.WriteLine("--------------------------\n" +
                "FastTree predictions:" + "\n" +
                $"R^2: {metrics.RSquared}" +
                $"\n--------------------------");
            #endregion Evaluation

            //Prediction Engine
            var predictorFastTree = context.Model.CreatePredictionEngine<InputTest, Output>(modelFastTree);

            #region hardcode prediction
            var house = new InputTest
            {
                Id = 1,
                MSSubClass = 50,
                MSZoning = "RH",
                LotArea = 8482,
                Street = "Pave",
                Alley = "Grvl",
                Utilities = "AllPub",
                Neighborhood = "Gilbert",
                Condition1 = "RRNn",
                BldgType = "Duplx",
                HouseStyle = "2.5Fin",
                OverallQual = 7,
                OverallCond = 8,
                YearBuilt = 2003,
                YearRemodAdd = 2003,
                ExterQual = "Gd",
                ExterCond = "Ex",
                BsmtQual = "Gd",
                BsmtCond = "TA",
                Heating = "Floor",
                HeatingQC = "TA",
                Electrical = "FuseA",
                KitchenQual = "Ex",
                MoSold = 4,
                YrSold = 2014,
                SaleType = "ConLw",
                SaleCondition = "Normal"

            };

            var hardPrediction = predictorFastTree.Predict(house);

            Console.WriteLine($"--------------------------\n" +
                $"Id: {hardPrediction.Id}\n" +
                $"Score: {hardPrediction.Score}\n" +
                $"\n--------------------------");
            #endregion hardcode prediction

            //Test prediction

            //StreamReader to get .csv data
            List<InputTest> testList = new List<InputTest>();
            bool first = true;
            string testFilePath = "./Data/test.csv";

            using (var reader = new StreamReader(testFilePath))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var data = line.Split(',');

                    //skips first line, as it only contains column names (program breaks if not done because of wrong value assignments)
                    if (first)
                    {
                        first = false;
                        continue;
                    }


                    InputTest newHouse = new InputTest
                    {
                        Id = float.Parse(data[0]),
                        MSSubClass = float.Parse(data[1]),
                        MSZoning = data[2],
                        LotArea = float.Parse(data[4]),
                        Street = data[5],
                        Alley = data[6],
                        Utilities = data[9],
                        Neighborhood = data[12],
                        Condition1 = data[13],
                        BldgType = data[15],
                        HouseStyle = data[16],
                        OverallQual = float.Parse(data[17]),
                        OverallCond = float.Parse(data[18]),
                        YearBuilt = float.Parse(data[19]),
                        YearRemodAdd = float.Parse(data[20]),
                        ExterQual = data[27],
                        ExterCond = data[28],
                        BsmtQual = data[30],
                        BsmtCond = data[31],
                        Heating = data[39],
                        HeatingQC = data[40],
                        Electrical = data[42],
                        KitchenQual = data[53],
                        MoSold = float.Parse(data[76]),
                        YrSold = float.Parse(data[77]),
                        SaleType = data[78],
                        SaleCondition = data[79]
                    };

                    testList.Add(newHouse);
                }

                //prediction of test-data and writing to output file
                string outputFilePath = "./Data/FastTree.csv";
                if (File.Exists(outputFilePath))
                {
                    File.Delete(outputFilePath);
                }

                first = true;
                foreach (InputTest houses in testList)
                {
                    var testPrediction = predictorFastTree.Predict(houses);

                    try
                    {
                        using (StreamWriter file = new StreamWriter(outputFilePath, true))
                        {
                            if (first)
                            {
                                first = false;
                                file.WriteLine("Id,SalePrice");
                            }
                            file.WriteLine(testPrediction.Id + "," + (int)Math.Round(testPrediction.Score));
                        }
                    }
                    catch (Exception e)
                    {

                        throw new ApplicationException("Error: " + e);
                    }
                }
            }
        }
    }
}
