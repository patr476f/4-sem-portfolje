﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace HousePricesML.DataClasses
{
    public class InputTest
    {
        /* Column names with Column number:
         * Id 0,                MSSubClass 1    MSZoning 2          LotFrontage 3   LotArea 4,      Street 5        Alley 6         LotShape 7      LandContour 8       Utilities 9
         * LotConfig 10         LandSlope 11    Neighborhood 12,    Condition1 13   Condition2 14   BldgType 15     HouseStyle 16   OverallQual 17  OverallCond 18      YearBuilt 19  
         * YearRemodAdd 20      RoofStyle 21,   RoofMatl 22,        Exterior1st 23  Exterior2nd 24  MasVnrType 25   MasVnrArea 26   ExterQual 27    ExterCond 28        Foundation 29 
         * BsmtQual 30          BsmtCond 31     BsmtExposure 32     BsmtFinType1 33 BsmtFinSF1 34   BsmtFinType2 35 BsmtFinSF2 36   BsmtUnfSF 37    TotalBsmtSF 38      Heating 39   
         * HeatingQC 40         CentralAir 41   Electrical 42       1stFlrSF 43     2ndFlrSF 44     LowQualFinSF 45 GrLivArea 46    BsmtFullBath 47 BsmtHalfBath 48     FullBath 49
         * HalfBath 50          BedroomAbvGr 51 KitchenAbvGr 52     KitchenQual 53  TotRmsAbvGrd 54 Functional 55   Fireplaces 56   FireplaceQu 57  GarageType 58       GarageYrBlt 59  
         * GarageFinish 60      GarageCars 61   GarageArea 62       GarageQual 63   GarageCond 64   PavedDrive 65   WoodDeckSF 66   OpenPorchSF 67  EnclosedPorch 68    3SsnPorch 69
         * ScreenPorch 70       PoolArea 71     PoolQC 72           Fence 73        MiscFeature 74  MiscVal 75      MoSold 76       YrSold 77       SaleType 78         SaleCondition 79
         * 
         * For info about the Columns and their data look at data_description.txt
         */

        [LoadColumn(0), ColumnName("Id")]
        public float Id { get; set; }

        [LoadColumn(1)]
        public float MSSubClass { get; set; }

        [LoadColumn(2)]
        public string MSZoning { get; set; }

        [LoadColumn(4)]
        public float LotArea { get; set; }

        [LoadColumn(5)]
        public string Street { get; set; }

        [LoadColumn(6)]
        public string Alley { get; set; }

        [LoadColumn(9)]
        public string Utilities { get; set; }

        [LoadColumn(12)]
        public string Neighborhood { get; set; }

        [LoadColumn(13)]
        public string Condition1 { get; set; }

        [LoadColumn(15)]
        public string BldgType { get; set; }

        [LoadColumn(16)]
        public string HouseStyle { get; set; }

        [LoadColumn(17)]
        public float OverallQual { get; set; }

        [LoadColumn(18)]
        public float OverallCond { get; set; }

        [LoadColumn(19)]
        public float YearBuilt { get; set; }

        [LoadColumn(20)]
        public float YearRemodAdd { get; set; }

        [LoadColumn(27)]
        public string ExterQual { get; set; }

        [LoadColumn(28)]
        public string ExterCond { get; set; }

        [LoadColumn(30)]
        public string BsmtQual { get; set; }

        [LoadColumn(31)]
        public string BsmtCond { get; set; }

        [LoadColumn(39)]
        public string Heating { get; set; }

        [LoadColumn(40)]
        public string HeatingQC { get; set; }

        [LoadColumn(42)]
        public string Electrical { get; set; }

        [LoadColumn(53)]
        public string KitchenQual { get; set; }

        [LoadColumn(76)]
        public float MoSold { get; set; }

        [LoadColumn(77)]
        public float YrSold { get; set; }

        [LoadColumn(78)]
        public string SaleType { get; set; }

        [LoadColumn(79)]
        public string SaleCondition { get; set; }
    }
}
