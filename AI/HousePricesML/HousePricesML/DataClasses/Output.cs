﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace HousePricesML.DataClasses
{
    public class Output
    {
        //Class that holds the output parameters for prediction engine.
        [ColumnName("Id")]
        public float Id { get; set; }

        public float Score { get; set; }
    }
}
