﻿using Benzin_pumpe.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Benzin_pumpe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new GasStationController();
        }

        //Generel Click method used for every button.
        //The button object is passed with the update method, so no specific method required for each button
        //as information about the button (and thereby its function) is found with the object itself and used within the states
        private void Generel_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is GasStationController controller)
            {
                controller.Update(sender);
            }
        }
    }
}
