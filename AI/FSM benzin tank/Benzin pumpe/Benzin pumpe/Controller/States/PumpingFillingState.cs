﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Controls;

namespace Benzin_pumpe.Controller.States
{
    public class PumpingFillingState : BaseState
    {
        public override void OnStart(GasStationController controller)
        {
            controller.InfoText = "Påfyldning er begyndt.";

            controller.BeginFillThread();
        }

        public override void Action(GasStationController controller)
        {

        }

        public override void Action(GasStationController controller, object sender)
        {
            if (sender is Button b)
            {
                if (b.Name == "Push_Stop")
                {
                    controller.InfoText = "Påfyldning stopper.";
                    controller.KillFillThread();
                    controller.TransitionState(controller._PumpingIdleState);
                }
            }
        }
    }
}
