﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace Benzin_pumpe.Controller.States
{
    public class GasTypeState : BaseState
    {
        public override void OnStart(GasStationController controller)
        {
            controller.InfoText = "Vælg benzin type der skal tankes op med.\n" +
                "Blyfri 92: 8.49 dkk pr. liter\n" +
                "Blyfri 95: 8.79 dkk pr. liter\n" +
                "Diesel: 8.12 dkk pr. liter\n" +
                "Derefter tag fat i håndtaget og tank op.";
        }

        public override void Action(GasStationController controller)
        {

        }

        public override void Action(GasStationController controller, object sender)
        {
            if (sender is Button b)
            {
                if (b.Name == "TypeLow")
                {
                    controller.GasType = GasTypes.Blyfri92;
                    controller.LiterPrice = 8.49f;
                    controller.InfoText = "Blyfri 92 er valgt.\n" +
                        "Tag fat i håndtaget for at starte med at tanke op.";
                }
                else if (b.Name == "TypeHigh")
                {
                    controller.GasType = GasTypes.Blyfri95;
                    controller.LiterPrice = 8.79f;
                    controller.InfoText = "Blyfri 95 er valgt.\n" +
                        "Tag fat i håndtaget for at starte med at tanke op.";
                }
                else if (b.Name == "TypeDiesel")
                {
                    controller.GasType = GasTypes.Diesel;
                    controller.LiterPrice = 8.12f;
                    controller.InfoText = "Diesel er valgt.\n" +
                        "Tag fat i håndtaget for at starte med at tanke op.";
                }
                else if (b.Name == "Lift_PutDown" && controller.GasType != GasTypes.None)
                {
                    controller.TransitionState(controller._PumpingIdleState);
                }
                else
                {
                    controller.InfoText = "Vælg venligst en benzin type.";
                }
            }
        }
    }
}
