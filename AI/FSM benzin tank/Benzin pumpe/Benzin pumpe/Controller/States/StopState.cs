﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Benzin_pumpe.Controller.States
{
    public class StopState : BaseState
    {
        public override void OnStart(GasStationController controller)
        {
            //Stop/Terminates any currently running threads to ensure no threads are running in background
            //This needs to be reworked so the correct flow is applied to gas filling. This will be done at a later date.
            if (controller.GasFillingThread is Thread && controller.GasFillingThread.IsAlive)
            {
                controller.KillFillThread();
            }
            if (controller.GasIdleThread is Thread && controller.GasIdleThread.IsAlive)
            {
                controller.KillIdleThread();
            }

            //sleep to ensure all values are reset. Lazy fix, but works atm.
            Thread.Sleep(100);

            controller.OnStart();
            controller.TransitionState(controller._IdleState);
        }

        public override void Action(GasStationController controller)
        {

        }

        public override void Action(GasStationController controller, object sender)
        {

        }
    }
}
