﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Benzin_pumpe.Controller.States
{
    public abstract class BaseState
    {
        public abstract void OnStart(GasStationController controller);
        public abstract void Action(GasStationController controller); //method that controls any actions made in the state
        public abstract void Action(GasStationController controller, object sender); //overload used to enable button parameter (object used for generel use)

    }
}
