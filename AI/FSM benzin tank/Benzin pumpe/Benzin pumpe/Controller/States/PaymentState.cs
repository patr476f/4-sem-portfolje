﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace Benzin_pumpe.Controller.States
{
    public class PaymentState : BaseState
    {
        public override void OnStart(GasStationController controller)
        {
            controller.InfoText = $"Der er valgt {controller._PaymentMethod}.\n";
            if (controller._PaymentMethod == PaymentMethod.Card)
            {
                controller.InfoText += "Tryk 'Godkend' for at gå videre eller Stop for at afslutte.";
            }
            else if (controller._PaymentMethod == PaymentMethod.Cash)
            {
                controller.InfoText += "Indsæt ønskede beløb for opfyldningen.\n" +
                    "Tryk derefter 'Godkend' for at gå videre eller Stop for at afslutte.";
            }
        }

        public override void Action(GasStationController controller)
        {

        }

        public override void Action(GasStationController controller, object sender)
        {
            if (sender is Button b)
            {
                if (b.Name == "Coin" && controller._PaymentMethod == PaymentMethod.Cash)
                {
                    controller.InsertedCash += 10f;
                    controller.InfoText = $"Der er indsat 10 dkk og totale beløb er: {controller.InsertedCash}.\n" +
                        $"Tryk 'Godkend' for at gå videre.";
                }
                else if (b.Name == "Note" && controller._PaymentMethod == PaymentMethod.Cash)
                {
                    controller.InsertedCash += 100f;
                    controller.InfoText = $"Der er indsat 100 dkk og totale beløb er: {controller.InsertedCash}.\n" +
                        $"Tryk 'Godkend' for at gå videre.";
                }
                else if (b.Name == "Confirm_Payment" && controller._PaymentMethod != PaymentMethod.None)
                {
                    if (controller._PaymentMethod == PaymentMethod.Cash && controller.InsertedCash <= 0)
                    {
                        controller.InfoText = "Indsæt venligt penge eller skift til kort, før du fortsætter.";
                    }
                    else
                    {
                        controller.TransitionState(controller._GasTypeState);
                    }
                }
            }
        }
    }
}
