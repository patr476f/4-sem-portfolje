﻿using System.Threading;
using System.Windows.Controls;

namespace Benzin_pumpe.Controller.States
{
    public class PumpingIdleState : BaseState
    {
        public override void OnStart(GasStationController controller)
        {
            controller.InfoText = "Tryk på håndtaget for at starte påfyldningen.\n" +
                "Læg håndtaget tilbage hvis påflydningen er færdig.";
            controller.BeginIdleThread();
        }

        public override void Action(GasStationController controller)
        {

        }

        public override void Action(GasStationController controller, object sender)
        {
            controller.KillIdleThread();
            if (sender is Button b)
            {
                if (b.Name == "Push_Stop")
                {
                    controller.TransitionState(controller._PumpingFillingState);
                }
                else if (b.Name == "Lift_PutDown")
                {
                    controller.TransitionState(controller._StopState);
                }
                else
                {
                    controller.BeginIdleThread();
                }
            }
        }
    }
}
