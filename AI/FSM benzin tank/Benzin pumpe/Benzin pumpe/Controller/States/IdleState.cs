﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace Benzin_pumpe.Controller.States
{
    public class IdleState : BaseState
    {
        public override void OnStart(GasStationController controller)
        {
            controller.InfoText = "Pumpen er klar til brug." +
                "\n" +
                "Vælg betalings metode: Kort eller Kontant.";
        }

        public override void Action(GasStationController controller)
        {

        }

        public override void Action(GasStationController controller, object sender)
        {
            if (sender is Button b)
            {
                if (b.Name == "Card")
                {
                    controller._PaymentMethod = PaymentMethod.Card;
                    controller.TransitionState(controller._PaymentState);
                }
                else if (b.Name == "Cash")
                {
                    controller._PaymentMethod = PaymentMethod.Cash;
                    controller.TransitionState(controller._PaymentState);
                }
                else
                {
                    controller.InfoText = "Vælg venligst en betalingsmetode først.\n" +
                            "Kort eller kontant.";
                }
            }
        }
    }
}
