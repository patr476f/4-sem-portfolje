﻿using Benzin_pumpe.Controller.States;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Controls;
using System.Threading;

namespace Benzin_pumpe.Controller
{
    //Enum for different types of payment method.
    public enum PaymentMethod
    {
        None,
        Card,
        Cash
    }

    public enum GasTypes
    {
        None,
        Blyfri92,
        Blyfri95,
        Diesel
    }

    //Remember to use INotifyPropertyChanged or the UI won't get updated
    public class GasStationController : INotifyPropertyChanged
    {
        private BaseState currentState;
        public BaseState CurrentState { get { return currentState; } }

        #region state field
        public readonly IdleState _IdleState = new IdleState();
        public readonly PaymentState _PaymentState = new PaymentState();
        public readonly GasTypeState _GasTypeState = new GasTypeState();
        public readonly PumpingIdleState _PumpingIdleState = new PumpingIdleState();
        public readonly PumpingFillingState _PumpingFillingState = new PumpingFillingState();
        public readonly StopState _StopState = new StopState();
        #endregion state field

        #region properties
        private string infoText;
        public string InfoText { get { return infoText; } set { infoText = value; OnPropertyChanged("InfoText"); } }

        private GasTypes gasType;
        public GasTypes GasType { get { return gasType; } set { gasType = value; OnPropertyChanged("GasType"); } }

        private float literPrice;
        public float LiterPrice { get { return literPrice; } set { literPrice = value; OnPropertyChanged("LiterPrice"); } }

        private float totalLiter;
        public float TotalLiter { get { return totalLiter; } set { totalLiter = value; OnPropertyChanged("TotalLiter"); } }

        private float totalPrice;
        public float TotalPrice { get { return totalPrice; } set { totalPrice = value; OnPropertyChanged("TotalPrice"); } }

        private float flow;
        public float Flow { get { return flow; } set { flow = value; OnPropertyChanged("Flow"); } }

        private float insertedCash;
        public float InsertedCash { get { return insertedCash; } set { insertedCash = value; OnPropertyChanged("InsertedCash"); } }

        //Properties used for functional purposes and not displayed in UI
        public PaymentMethod _PaymentMethod;

        public Thread GasFillingThread;
        public Thread GasIdleThread;
        private bool GasFillingThreadBool;
        private bool GasIdleThreadBool;
        #endregion properties

        public void BeginFillThread()
        {
            GasFillingThreadBool = true;
            GasFillingThread = new Thread(GasFilling);
            GasFillingThread.Start();
        }

        public void KillFillThread()
        {
            GasFillingThreadBool = false;
        }

        private void GasFilling()
        {
            //Update rate is how many times pr. second the values are updated
            int updateRate = 10;

            Flow = 0.05f;
            for (int i = 0; i < 2; i++)
            {
                TotalLiter += Flow / updateRate;
                TotalPrice += LiterPrice * Flow / updateRate;
                Thread.Sleep(1000 / updateRate);
            }

            Flow = 0.3f;
            while (GasFillingThreadBool)
            {
                Thread.Sleep(1000 / updateRate);
                if (GasFillingThreadBool)
                {
                    if (_PaymentMethod == PaymentMethod.Cash)
                    {
                        if ((InsertedCash - TotalPrice) < (((LiterPrice * Flow) / updateRate)))
                        {
                            GasFillingThreadBool = false;
                        }
                        else
                        {
                            TotalLiter += Flow / updateRate;
                            TotalPrice += (LiterPrice * Flow) / updateRate;
                        }
                    }
                    else
                    {
                        TotalLiter += Flow / updateRate;
                        TotalPrice += (LiterPrice * Flow) / updateRate;
                    }
                    
                }
            }

            Flow = 0.05f;
            for (int i = 0; i < 1; i++)
            {
                TotalLiter += Flow / updateRate;
                TotalPrice += LiterPrice * Flow / updateRate;
                Thread.Sleep(1000 / updateRate);
            }
            Flow = 0f;
        }

        public void BeginIdleThread()
        {
            GasIdleThreadBool = true;
            GasIdleThread = new Thread(GasIdleStop);
            GasIdleThread.Start();
        }

        public void KillIdleThread()
        {
            GasIdleThreadBool = false;
        }

        private void GasIdleStop()
        {
            while (GasIdleThreadBool)
            {
                Thread.Sleep(5000);

                //Extra check to ensure Stop-State isn't run (for some reason the while loop continues even when GasIdleThreadBool is false)
                //This may be caused by the Thread not having updated the bool, but it may also be just how while-loops function.
                if (GasIdleThreadBool)
                {
                    TransitionState(_StopState);
                }
            }
        }

        public GasStationController()
        {
            OnStart();

            currentState = _IdleState;
            currentState.OnStart(this);
        }

        public void OnStart()
        {
            InfoText = "This is a info panel to help users";
            GasType = GasTypes.None;
            LiterPrice = 0f;
            TotalLiter = 0f;
            TotalPrice = 0f;
            Flow = 0f;

            GasFillingThreadBool = false;
            GasIdleThreadBool = false;

            _PaymentMethod = PaymentMethod.None;
            InsertedCash = 0f;
        }

        //Method used whenever an update happens (manually called at 'events')
        public void Update()
        {
            currentState.Action(this);
        }
        //Overload method used to parse button object, so action can be made depending on the button pressed. This may be reworked at a later date.
        public void Update(object sender)
        {
            //Temporary solution for STOP button (will get reworked if needed later)
            if (sender is Button b)
            {
                if (b.Name == "STOP")
                {
                    TransitionState(_StopState);
                }
                else
                {
                    currentState.Action(this, sender);
                }
            }
        }

        public void TransitionState(BaseState newState)
        {
            currentState = newState;
            currentState.OnStart(this);
        }

        #region PropertyChangedEventhandler

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion PropertyChangedEventhandler
    }
}
