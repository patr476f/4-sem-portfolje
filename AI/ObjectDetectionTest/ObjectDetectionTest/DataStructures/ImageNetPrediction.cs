﻿using Microsoft.ML.Data;

namespace ObjectDetectionTest.DataStructures
{
    public class ImageNetPrediction
    {
        [ColumnName("grid")]
        public float[] PredictedLabels;
    }
}
