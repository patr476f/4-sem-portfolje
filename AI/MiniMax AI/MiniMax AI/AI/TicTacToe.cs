﻿using System;
using System.Text;

namespace MiniMax_AI.AI
{
    public class TicTacToe
    {
        // This Class is built with the Evaluation function as a principle.
        // This class is also designed from scratch, whereas the GeekforGeek guide is going to be built in another class.

        // This class fails on a fundamental level.
        // While this class is capable of playing tic tac toe, with an AI, it can easily be duped by 'advanced' tactics.
        // This happens as the 'AI' designed in this class doesn't look forward and only observes the current game state.
        // Meaning any moves the player makes, which 'traps' the AI by enabling multi win moves (having 2 options at once for win placements), are achievable
        // The reason for this, is also based on the priority system in use. As the system 'rewards' position based on threat and reward,
        // where player positions are negativ and ai positions are positive, means certain positions aren't as 'good' in the eyes of the AI.
        // This allows for easy victories, by gaining corner positions more easily.
        // This can easily be succumbvented by creating minimax algoritmes, that look forward in game states, as well as creating a more suitable point system for picking positions.

        // Instead of using a algoritme that holds every single solution, this AI will pick based on number and chances of losing.
        // the board will have this value assignments:
        // 3 | 2 | 3
        // 2 | 4 | 2
        // 3 | 2 | 3
        // based on the number of winning positions the position is part of.
        // Any turns, where a win or a lose is possible, the needed position is multiplied by a number to increase its value.
        // this number is the different picked positions, where AI picked positions are positive and player picked are negative.
        // if no positions are picked all surrounding positions are all neglected

        // this function evaluates the board, where the input is a matrix that holds int for which player controls the position.
        // 0 = none, 1 = cpu & -1 = player
        // example for matrix: {{0, 1, -1},{0, 1, 0}, {-1, 0, 0}}
        private int[,] AIMove(int[,] board)
        {
            int[,] b = board;
            int corner = 3;
            int mid = 4;
            int split = 2;
            int[,] valueB = { { corner, split, corner }, { split, mid, split }, { corner, split, corner } };

            int[] pos = { 0, 0 };
            int value = 0;
            int currentValue = 0;

            int minNeg = -2;
            int minPos = 8;

            //runs every position through, checking for best value
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    //checks if position is empty
                    if (b[row, col] == 0)
                    {
                        //checking row value
                        value = valueB[row, col] + b[row, 0] * valueB[row, 0] + b[row, 1] * valueB[row, 1] + b[row, 2] * valueB[row, 2];
                        if (currentValue < minPos && value >= minPos)
                        {
                            pos[0] = row;
                            pos[1] = col;
                            currentValue = value;
                        }
                        else if (currentValue > minNeg && value <= minNeg)
                        {
                            pos[0] = row;
                            pos[1] = col;
                            currentValue = value;
                        }
                        else if (value > currentValue && (currentValue <= minPos && currentValue >= minNeg))
                        {
                            pos[0] = row;
                            pos[1] = col;
                            currentValue = value;
                        }

                        //checks col value
                        value = valueB[row, col] + b[0, col] * valueB[0, col] + b[1, col] * valueB[1, col] + b[2, col] * valueB[2, col];
                        if (currentValue < minPos && value >= minPos)
                        {
                            pos[0] = row;
                            pos[1] = col;
                            currentValue = value;
                        }
                        else if (currentValue > minNeg && value <= minNeg)
                        {
                            pos[0] = row;
                            pos[1] = col;
                            currentValue = value;
                        }
                        else if (value > currentValue && (currentValue <= minPos && currentValue >= minNeg))
                        {
                            pos[0] = row;
                            pos[1] = col;
                            currentValue = value;
                        }

                        // if it is in a diagonal position
                        if (row == col)
                        {
                            value = valueB[row, col] + b[0, 0] * valueB[0, 0] + b[1, 1] * valueB[1, 1] + b[2, 2] * valueB[2, 2];
                            if (currentValue < minPos && value >= minPos)
                            {
                                pos[0] = row;
                                pos[1] = col;
                                currentValue = value;
                            }
                            else if (currentValue > minNeg && value <= minNeg)
                            {
                                pos[0] = row;
                                pos[1] = col;
                                currentValue = value;
                            }
                            else if (value > currentValue && (currentValue <= minPos && currentValue >= minNeg))
                            {
                                pos[0] = row;
                                pos[1] = col;
                                currentValue = value;
                            }
                        }
                        if ((row == 2 && col == 0) || (row == 1 && col == 1) || (row == 0 && col == 2))
                        {
                            value = valueB[row, col] + b[2, 0] * valueB[2, 0] + b[1, 1] * valueB[1, 1] + b[0, 2] * valueB[0, 2];
                            if (currentValue < minPos && value >= minPos)
                            {
                                pos[0] = row;
                                pos[1] = col;
                                currentValue = value;
                            }
                            else if (currentValue > minNeg && value <= minNeg)
                            {
                                pos[0] = row;
                                pos[1] = col;
                                currentValue = value;
                            }
                            else if (value > currentValue && (currentValue <= minPos && currentValue >= minNeg))
                            {
                                pos[0] = row;
                                pos[1] = col;
                                currentValue = value;
                            }
                        }
                    }
                }
            }

            b[pos[0], pos[1]] = 1;

            return b;
        }

        // This function is a proof of concept function. Where instead of valueing the position with the evaluation,
        // the position is instead only chosen by finding the position with most 'overall' value in terms of surrounding pieces.
        // This means that the actual positions value is only taken into account, when multiple/all other position share overall value.
        // Making this change for the decision making AI, should help pick the 'best' position while also allowing CPU to be either 1. or 2.
        // How this works is by still using the value assignment from above, with the cpu and player values 1 og -1.
        // These values are multiplied together, to represent player or CPU value, then added together for row, col and diagonal positions (if a diagonal win is possible for the position)
        // All the values are then added together, as positives, which should make a better representation of position overall value.
        private int[,] AIMoveTest(int[,] board)
        {
            // this function has better decision making than AIMove, as it values positions better.
            // however its still not as effective as a true minimax algoritme, as its still prone to lose to moves that look into the future and force a 2-way win
            int[,] b = board;

            int corner = 3;
            int mid = 4;
            int split = 2;
            int[,] valueB = { { corner, split, corner }, { split, mid, split }, { corner, split, corner } };

            int[,] value = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };

            //loop to find all position values
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    //checks for empty
                    if (b[row, col] == 0)
                    {
                        //row values
                        value[row, col] = Math.Abs(b[row, 0] * valueB[row, 0] + b[row, 1] * valueB[row, 1] + b[row, 2] * valueB[row, 2]);
                        //col values
                        value[row, col] += Math.Abs(b[0, col] * valueB[0, col] + b[1, col] * valueB[1, col] + b[2, col] * valueB[2, col]);

                        //diagonal if possible /
                        if (row == col)
                        {
                            value[row, col] += Math.Abs(b[0, 0] * valueB[0, 0] + b[1, 1] * valueB[1, 1] + b[2, 2] * valueB[2, 2]);
                        }
                        // diagonal \
                        if ((row == 2 && col == 0) || (row == 1 && col == 1) || (row == 0 && col == 2))
                        {
                            value[row, col] += Math.Abs(b[2, 0] * valueB[2, 0] + b[1, 1] * valueB[1, 1] + b[0, 2] * valueB[0, 2]);
                        }
                    }
                }
            }

            int valueCurrent = 0;
            int[] pos = { 0, 0 };
            int baseValueCurrent = 0; // base value, to chose position if multiple positions have same value
            // loop to decide cpu move
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    // if value is higher than current value, a new current is chosen and occurence is reset
                    if (value[row, col] > valueCurrent)
                    {
                        valueCurrent = value[row, col];
                        pos[0] = row;
                        pos[1] = col;
                        baseValueCurrent = valueB[row, col];
                    }
                    // new position is picked, if new position has same value and higher base value
                    else if (value[row, col] == valueCurrent && baseValueCurrent < valueB[row, col])
                    {
                        valueCurrent = value[row, col];
                        pos[0] = row;
                        pos[1] = col;
                        baseValueCurrent = valueB[row, col];
                    }
                }
            }

            //position is saved on board as CPU position
            b[pos[0], pos[1]] = 1;

            return b;
        }

        private int[,] PlayerTurn(int[,] board)
        {
            int[,] b = board;
            int row = 0;
            int col = 0;

            Console.WriteLine("Pick position");

            bool check = true;
            while (check)
            {
                Console.Write("Row: ");
                row = int.Parse(Console.ReadLine());
                Console.Write("Col: ");
                col = int.Parse(Console.ReadLine());

                if (row > 2 || row < 0)
                {
                    continue;
                }
                if (col > 2 || col < 0)
                {
                    continue;
                }

                if (b[row, col] == 0)
                {
                    b[row, col] = -1;
                    check = false;
                }
            }

            return b;
        }

        // checks if either player has won
        private bool WinCon(int[,] board)
        {
            bool res = false;

            //row checks
            for (int row = 0; row < 3; row++)
            {
                if (board[row, 0] == board[row, 1] && board[row, 1] == board[row, 2])
                {
                    if (board[row, 0] == 1)
                    {
                        Console.WriteLine("CPU has won!");
                        res = true;
                    }
                    else if (board[row, 0] == -1)
                    {
                        Console.WriteLine("Player has won!");
                        res = true;
                    }
                }
            }

            //col checks
            for (int col = 0; col < 3; col++)
            {
                if (board[0, col] == board[1, col] && board[1, col] == board[2, col])
                {
                    if (board[0, col] == 1)
                    {
                        Console.WriteLine("CPU has won!");
                        res = true;
                    }
                    else if (board[0, col] == -1)
                    {
                        Console.WriteLine("Player has won!");
                        res = true;
                    }
                }
            }

            //diagonal checks
            if (board[0, 0] == board[1, 1] && board[1, 1] == board[2, 2])
            {
                if (board[1, 1] == 1)
                {
                    Console.WriteLine("CPU has won!");
                    res = true;
                }
                else if (board[1, 1] == -1)
                {
                    Console.WriteLine("Player has won!");
                    res = true;
                }
            }
            if (board[0, 2] == board[1, 1] && board[1, 1] == board[2, 0])
            {
                if (board[1, 1] == 1)
                {
                    Console.WriteLine("CPU has won!");
                    res = true;
                }
                else if (board[1, 1] == -1)
                {
                    Console.WriteLine("Player has won!");
                    res = true;
                }
            }

            //checks for draw
            bool draw = true;
            for (int row = 0; row < 3 && draw == true; row++)
            {
                for (int col = 0; col < 3 && draw == true; col++)
                {
                    // if any positions are open, draw is false and game continues
                    if (board[row, col] == 0)
                    {
                        draw = false;
                    }
                }
            }
            if (draw)
            {
                Console.WriteLine("It's a draw!");
                res = true;
            }

            return res;
        }

        private void PlotBoard(int[,] board)
        {
            int pos;
            for (int row = 0; row < 4; row++)
            {
                if (row == 0)
                {
                    Console.WriteLine("     0   1   2");
                    continue;
                }
                for (int col = 0; col < 4; col++)
                {
                    if (col == 0)
                    {
                        Console.Write($" {row - 1}: ");
                        continue;
                    }
                    pos = board[row - 1, col - 1];
                    if (pos == 1)
                    {
                        Console.Write(" O ");
                    }
                    else if (pos == -1)
                    {
                        Console.Write(" X ");
                    }
                    else
                    {
                        Console.Write("   ");
                    }

                    if (col == 1 || col == 2)
                    {
                        Console.Write("|");
                    }
                    if (col == 3)
                    {
                        Console.WriteLine();
                    }
                }
            }
        }

        // play function, that starts the game with 1 player and 1 cpu.
        public void PlayTicTacToe()
        {
            bool loop = true;
            int[,] board = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };

            //loop for play:
            while (loop)
            {
                Console.Clear();
                PlotBoard(board);

                board = PlayerTurn(board);
                if (!WinCon(board))
                {
                    //board = AIMove(board);
                    board = AIMoveTest(board);
                    loop = !WinCon(board);
                }
                else
                {
                    loop = false;
                }

                if (!loop)
                {
                    PlotBoard(board);
                }
            }
        }
    }
}
