﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniMax_AI.AI
{
    public class TreeAI
    {
        //This class is built with the code from this guide: https://www.geeksforgeeks.org/minimax-algorithm-in-game-theory-set-1-introduction/

        //Maximizer function that return most optimal value, in a game tree (with 2 options each branching).
        // depth indicate the current depth of the tree
        // nodeIndex is index of the current node in []scores, aka which number is currently chosen
        // isMax indicate if the current running of the function is maximizer, if not its false
        // scores[] stores the leaves of the Game Tree
        // height is the maximum height of the tree.
        public int MiniMax(int depth, int nodeIndex, bool isMax, int[]scores, int height)
        {
            // Stops the function if leaf node is reached, aka no more branches
            if (depth == height)
            {
                return scores[nodeIndex];
            }

            // If function is checking for maximizer value, it'll find maximum value optainable 
            if (isMax)
            {
                return Math.Max(MiniMax(depth + 1, nodeIndex * 2, false, scores, height),
                    MiniMax(depth + 1, nodeIndex * 2 + 1, false, scores, height));
            }

            // finds min value instead
            else
            {
                return Math.Min(MiniMax(depth + 1, nodeIndex * 2, true, scores, height),
                    MiniMax(depth + 1, nodeIndex * 2 + 1, true, scores, height));
            }

            // The functions runs down all branches of the game tree, and interchanges Min and Max to 'simulate' players.
            // It continues to do this until the function reaches the buttom of the tree, where all the index' now match the different leafs (values)
            // All values are returned to previous steps of the function, where a Mininum or Maximum value is chosen depending on which 'player' turn it is.

            // problems for this function is that it only takes account for 2 option branching.
            // this is a problem, since most games allows way more branching options for players to pick.
            // however it does showcase the basic type of MiniMax algoritms
        }

        // function to find height of tree based on Log in math
        private int log2(int n)
        {
            return (n == 1) ? 0 : 1 + log2(n / 2);
        }

        public void RunMiniMax(int[] scores)
        {
            int h = log2(scores.Length);
            int res = MiniMax(0, 0, true, scores, h);
            Console.WriteLine("Optimal value: " + res);
        }
    }
}
