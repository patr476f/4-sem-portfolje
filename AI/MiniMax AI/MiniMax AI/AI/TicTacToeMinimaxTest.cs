﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniMax_AI.AI
{
    public enum players
    {
        None,
        Player,
        CPU
    }

    public class TicTacToeMinimaxTest
    {
        // This class will be made using the geeksforgeeks minimax algoritme.
        // this is mostly copy work, but will act as a 'experience' to help remembering how to build future minimax algoritmes.
        // For indepth descriptions look at TicTacToeMinMax class.
        // furthermore this class will also allow actual gameplay for Tic tac toe with the algoritm.

        // Firstly some core methods are needed:
        /*
         * Evaluation method, to give points depending on which winning board is present
         * Minimax method, that keeps calling itself, alternating between max and min results (to represent player moves), in order to find all possible solutions and therefore best move
         * Best move method, that outputs the best output, when taking the current board state into account. This method also calls the minimax method.
         * Draw/full board method, that simply checks if the board is filled out
         * Run+play methods, that just allows player to make moves and for easy execution of the class
         */

        //Checks which player has won
        public int Evaluate(players[,] board)
        {
            //This value represent the value of the board,
            /*
             * 0 if no winners
             * 10 if cpu wins
             * -10 if player wins
             */
            int value = 0;

            for (int i = 0; i < 3; i++)
            {
                //checks whether a row or column is full of a specific player value
                // first part checks rows, the last part checks for columns
                if ((board[i, 0] == board[i, 1] && board[i, 1] == board[i, 2]))
                {
                    if (board[i, 0] == players.Player)
                    {
                        value = -10;
                    }
                    else if (board[i, 0] == players.CPU)
                    {
                        value = 10;
                    }
                }
                else if ((board[0, i] == board[1, i] && board[1, i] == board[2, i]))
                {
                    if (board[0, i] == players.Player)
                    {
                        value = -10;
                    }
                    else if (board[0, i] == players.CPU)
                    {
                        value = 10;
                    }
                }
            }

            //checks for diagonals
            if ((board[0, 0] == board[1, 1] && board[1, 1] == board[2, 2]) ||
                (board[0, 2] == board[1, 1] && board[1, 1] == board[2, 0]))
            {
                if (board[1, 1] == players.Player || board[1, 1] == players.Player)
                {
                    value = -10;
                }
                else if (board[1, 1] == players.CPU || board[1, 1] == players.CPU)
                {
                    value = 10;
                }
            }

            return value;
        }

        //checks if more available moves are left (is there a empty space on the board)
        public bool isMoveLeft(players[,] board)
        {
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    if (board[row, col] == players.None)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        //minimax algoritme, that checks board and chooses value based on maximizer or minimizer bool
        // Aka if its the AI's turn it will maximize and then alternate when checking outcomes.
        public int Minimax(players[,] board, int depth, bool isMax)
        {
            //checks the value of the current board
            int value = Evaluate(board);

            //returns value if a winner i found
            // Depth is added/attracted to ensure quick victories aren't toppled by later victories
            // as the longer a game goes on the less valuable a victory becomes.
            // CPU winner
            if (value == 10)
            {
                return value - depth;
            }
            // Player winner
            if (value == -10)
            {
                return value + depth;
            }

            //return 0 if no more moves are left and its a draw
            if (!isMoveLeft(board))
            {
                return 0;
            }

            //Maximizer decision making
            if (isMax)
            {
                // a large start value is chosen, to ensure the first max goes to minimax value
                // later the 'best' value will be overwritten based on value of a move.
                int best = -2000;

                //checks every possible move left on board for more minimaxing
                for (int row = 0; row < 3; row++)
                {
                    for (int col = 0; col < 3; col++)
                    {
                        if (board[row, col] == players.None)
                        {
                            board[row, col] = players.CPU;

                            best = Math.Max(best, Minimax(board, depth + 1, !isMax));

                            //move is undone, as it's only needed for decision making not actual move (yet)
                            board[row, col] = players.None;
                        }
                    }
                }

                return best;
            }
            else
            {
                //if its the players turn, in the decision making part
                // this statement runs, only difference from previous one is that its player value and finding min value when picking
                int best = 2000;

                for (int row = 0; row < 3; row++)
                {
                    for (int col = 0; col < 3; col++)
                    {
                        if (board[row, col] == players.None)
                        {
                            board[row, col] = players.Player;

                            best = Math.Min(best, Minimax(board, depth + 1, !isMax));

                            //move is undone, as it's only needed for decision making not actual move (yet)
                            board[row, col] = players.None;
                        }
                    }
                }

                return best;
            }
        }

        public Move FindBestMove(players[,] board)
        {
            //same principle as in minimax, however this function starts the algoritme
            Move bestMove = new Move();
            bestMove.row = -1;
            bestMove.col = -1;
            bestMove.value = -1000;
            // both row and col is just debug values, should be overwritten always unless no moves are available

            //now like in minimax, it tests all empty spaces on the board
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    if (board[row, col] == players.None)
                    {
                        board[row, col] = players.CPU;

                        int moveVal = Minimax(board, 0, false);

                        board[row, col] = players.None;

                        //checks if current move is better than current best move
                        if (bestMove.value < moveVal)
                        {
                            bestMove.row = row;
                            bestMove.col = col;
                            bestMove.value = moveVal;
                        }
                    }
                }
            }

            return bestMove;
        }

        //player turn method
        private players[,] PlayerTurn(players[,] board)
        {
            int row = 0;
            int col = 0;

            Console.WriteLine("Pick position");

            bool check = true;
            while (check)
            {
                Console.Write("Row: ");
                row = int.Parse(Console.ReadLine());
                Console.Write("Col: ");
                col = int.Parse(Console.ReadLine());

                if (row > 2 || row < 0)
                {
                    continue;
                }
                if (col > 2 || col < 0)
                {
                    continue;
                }

                if (board[row, col] == players.None)
                {
                    board[row, col] = players.Player;
                    check = false;
                }
            }

            return board;
        }

        private void PlotBoard(players[,] board)
        {
            players pos;
            for (int row = 0; row < 4; row++)
            {
                if (row == 0)
                {
                    Console.WriteLine("     0   1   2");
                    continue;
                }
                for (int col = 0; col < 4; col++)
                {
                    if (col == 0)
                    {
                        Console.Write($" {row - 1}: ");
                        continue;
                    }
                    pos = board[row - 1, col - 1];
                    if (pos == players.CPU)
                    {
                        Console.Write(" O ");
                    }
                    else if (pos == players.Player)
                    {
                        Console.Write(" X ");
                    }
                    else
                    {
                        Console.Write("   ");
                    }

                    if (col == 1 || col == 2)
                    {
                        Console.Write("|");
                    }
                    if (col == 3)
                    {
                        Console.WriteLine();
                    }
                }
            }
        }

        public void Play()
        {
            bool loop = true;
            players[,] board = { 
                { players.None, players.None, players.None }, 
                { players.None, players.None, players.None }, 
                { players.None, players.None, players.None } };

            while (loop)
            {
                PlotBoard(board);

                if (isMoveLeft(board))
                {
                    board = PlayerTurn(board);
                    if (Evaluate(board) != 0)
                    {
                        loop = false;
                        Console.Clear();
                        continue;
                    }
                }
                if (isMoveLeft(board))
                {
                    Move AIMove = FindBestMove(board);
                    board[AIMove.row, AIMove.col] = players.CPU;
                    if (Evaluate(board) != 0)
                    {
                        loop = false;
                        Console.Clear();
                        continue;
                    }
                }

                loop = isMoveLeft(board);

                Console.Clear();
            }

            //board plot
            //announce winner/outcome
            PlotBoard(board);
            if (Evaluate(board) == 10)
            {
                Console.WriteLine("CPU has won!");
            }
            else if (Evaluate(board) == -10)
            {
                Console.WriteLine("Player has won!");
            }
            else
            {
                Console.WriteLine("It's a draw!");
            }
            Console.WriteLine("------------------");
        }
    }
}
