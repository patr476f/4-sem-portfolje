﻿using System;
using MiniMax_AI.AI;

namespace MiniMax_AI
{
    class Program
    {
        static TreeAI GameTree = new TreeAI();
        static TicTacToe TTT = new TicTacToe();
        static TicTacToeMinMax ticMinMax = new TicTacToeMinMax();
        static TicTacToeMinimaxTest minimaxTest = new TicTacToeMinimaxTest();


        static void Main(string[] args)
        {
            minimaxTest.Play();

            int[] TreeAIScores = { 3, 5, 2, 9, 12, 5, 23, 23 };
            int[] TreeAIScores2 = { 22, 4, 21, 15, 9, 13, 20, 25 };

            GameTree.RunMiniMax(TreeAIScores);
            GameTree.RunMiniMax(TreeAIScores2);

            Console.WriteLine("Press 1 to play Tic Tac Toe or skip with any other input...");
            if (Console.ReadLine() == "1")
            {
                TTT.PlayTicTacToe();
            }

            Console.WriteLine("--------------------------");
            ticMinMax.RunTicTacToe();

            Console.ReadLine();
        }
    }
}
